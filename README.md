# airbnblike

1. Clone repository
2. `npm install`
3. Add this extension to Chrome and enable it when running the project: https://chrome.google.com/webstore/detail/allow-control-allow-origi/nlfbmbojpeacfghkpbjhddihlkkiljbi?hl=en
4. `npm start`
5. Enjoy!

## Last but not least:
* Don't forget to disable the extension when finish