import React from 'react';
import ReactDOM from 'react-dom';

// Styles
import style from './scss/main.scss';

// mobx
import { Provider } from 'mobx-react';

// Store
import appStore from './stores/store';

// react-router-dom
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

// App Component
import App from './components/app/app';
import Property from './components/property/property';

const appLocation = document.querySelector('.app');

const AppRouter = () => (
    <Router>
        <Switch>
            <Route path="/" exact component={App} />
            <Route path="/property" component={Property} />
        </Switch>
    </Router>
);

ReactDOM.render(
    <Provider store={appStore}>
        <AppRouter />
    </Provider>,
    appLocation
)