import React from 'react';
import { inject, observer } from 'mobx-react';
import {toJS} from 'mobx';

// Components
import Review from '../review/review';

@inject('store')
@observer
export default class PropertyReviews extends React.Component {
    showReviews(){
        const { propertyReviews } = this.props.store;

        return propertyReviews.map((review,index) => {
            const {author, created_at, comments} = review;

            return (
                <Review 
                    key={index}
                    username={author.first_name}
                    created_at={created_at}
                    profile_pic={author.picture_url}
                    comment={comments}
                />
            );
        })
    }

    render(){
        return(
            <div>
                <h2>Reviews:</h2>
                {this.showReviews()}
            </div>
        );
    }
}