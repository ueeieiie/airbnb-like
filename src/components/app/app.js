// React
import React from 'react';

import { observer, inject } from 'mobx-react'

// react-router-dom
import { withRouter } from 'react-router-dom';

// Styled-component
import styled from 'styled-components';

// import styles from 'normalize.css';
import normalize from './style.css';

// Components
import Menu from '../menu/menu';
import Properties from '../properties/properties';
// import Test from '../test/test';
import Pagination from '../pagination/pagination';

// Wrappers
import HeaderWrapper from './header.style';
import MainWrapper from './main.style'


// App Component
@inject('store')
@observer
export default class App extends React.Component {
    setPropertyInfo = (property) => {
        console.log('redirect to propertyInfo:');
    }

    render(){
        return(
            <div>
                <HeaderWrapper>
                    <h1>Choose City</h1>
                    <Menu cities={this.props.store.citiesList}/>
                    <Pagination />
                </HeaderWrapper>
                <MainWrapper>
                     <Properties fetchedProperties={this.props.store.fetchedProperties} />
                </MainWrapper>
            </div>
        );
    }
}
