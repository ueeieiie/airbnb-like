import styled from 'styled-components';

export default styled.div`
    padding: 15px 15px;
    margin-bottom: 40px;
    text-align: center;
`;