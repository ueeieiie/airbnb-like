import styled from 'styled-components';

export default styled.div`
    display: flex;
    justify-content: center;
    font-size: 25px;
    padding: 40px 0;

    .buttons {
        width: 100px;
        background-color: lightgray;
        border: 1px solid black;
        border-radius: 15px;        

        a {
            color: black;
            text-decoration: none;
            display: inline-block;
            width: 100%;
            line-height: 1.5;
        }
        
        &:hover {
            cursor: pointer;
            background-color: gray;
        }
        
        &.disabled {
            background-color: lightgray;
            cursor: not-allowed;

            a {
                pointer-events: none;            
            }
        }
    }
    
    .page_number {
        margin: 0 20px;
        padding: 5px 0;
    }
`;
