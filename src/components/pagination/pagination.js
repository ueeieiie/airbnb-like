import React from 'react';

import { inject, observer } from 'mobx-react';

import PaginationStyle from './pagination.style';


@inject('store')
@observer
export default class Pagination extends React.Component {
    previousPage = (e) => {
        e.preventDefault();
        this.props.store.updateResultsByPage('prev');
    }

    nextPage = (e) => {
        e.preventDefault();        
        this.props.store.updateResultsByPage('next');        
    }

    setPrevButton = () => {
        const { page } = this.props.store.pagination;

        return page == 1 ? 'buttons disabled' : 'buttons';
    }

    render(){
        return(
            <PaginationStyle>
            <div className={ this.setPrevButton()  }>
                <a href="" onClick={this.previousPage}>Prev</a>
            </div>

            <div className="page_number">
                {this.props.store.pagination.page}
            </div>

            <div className="buttons">
                <a href="" onClick={this.nextPage}>Next</a>
            </div>
            </PaginationStyle>
        );
    }
}