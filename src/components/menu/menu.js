import React from 'react';

import {observer, inject} from 'mobx-react'
// Component's style
import { MenuStyle } from './menu.style';

// Menu Component
@inject('store')
@observer
export default class Menu extends React.Component {
    setSelectedCity = (city) => {
        this.props.store.setSelectedCity(city);
    }

    createMenuOfCities = () => {
        const { cities } = this.props;
        const menu = [];

        for(let city in cities){
            menu.push(
                <option key={cities[city].id} value={city}>
                    {cities[city].title}
                </option>
            );
        }
        return menu;
    }

    render(){
        return(
            <form>
                <MenuStyle onChange={(e) => this.setSelectedCity(e.target.value)} >
                    {this.createMenuOfCities()}
                </MenuStyle>
            </form>
        );
    }
}