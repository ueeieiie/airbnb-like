// React
import React from 'react';

// Mobx
import {toJS} from 'mobx'

// Mobx-react
import {observer, inject} from 'mobx-react'

// Components
import Thumbnail from '../thumbnail/thumbnail';

@inject('store')
@observer
export default class Properties extends React.Component {
    showProperties = () => {
        const { fetchedProperties } = this.props;
        
        if (fetchedProperties.length) {
            return this.props.fetchedProperties.map((property, i) =>
                <Thumbnail key={i} info={property} />
            )
        } else return 'Loading...';
    }

    componentWillMount(){
        this.props.store.updateFetchedProperties();
    }

    render(){
        return( <div> { this.showProperties()} </div> );
    }
}