// React
import React from 'react';

// Mobx-react
import {inject, observer} from 'mobx-react';

// Mobx
import {toJS} from 'mobx';

import { withRouter } from 'react-router-dom';

// styles
import PropertyMainStyle from './propertyMain.style';
import PropertyHeaderStyle from './propertyHeader.style';

// Components
import PropertyReviews from '../propertyReviews/propertyReviews';

@inject('store')
@observer
class Property extends React.Component {

    addRatingStats(stars){
        const ratingElement = [];

        if(stars % 1 === 0) {
            for(let i = 1; i <= stars; i++) {
                ratingElement.push(
                    <i  key={i} className="rating-icon fa fa-star"  aria-hidden="true"></i>
                )
            }
        } else {
            for(let i = 1; i < stars; i++) {
                ratingElement.push(
                    <i key={i} className="rating-icon fa fa-star"  aria-hidden="true"></i>
                )
            }
            ratingElement.push(
                <i key={stars+1} className="rating-icon fa fa-star-half-o" aria-hidden="true"></i>
            )
        }

        return ratingElement;
    }

    showProperty(){

        if(this.props.store.propertyInfo){
            this.property = this.props.store.propertyInfo.listing;        
            const { 
                bathrooms, 
                bedrooms, 
                beds, 
                city, 
                extra_host_languages, 
                name, 
                person_capacity,
                picture_url,
                primary_host,
                property_type,
                public_address,
                star_rating, 
                user,
                xl_picture_url,
                room_type,
                localized_city,
                reviews_count
            } = this.property;

            return(
                <div className="property-wrapper">
                    <PropertyHeaderStyle style={{backgroundImage: `url(${xl_picture_url}`}}> 
                    </PropertyHeaderStyle>
                    
                    <PropertyMainStyle>
                        <main className="main">

                            <div className="main_info">
                                <div className="main_info_property_title_wrapper">
                                    <h2 className="main_info_property_title">{name}</h2>
                                </div>
                                <div className="main_info_core_info  main_info_section">
                                    <span className="main_info_core_info_topic">{room_type}</span>
                                    <span className="main_info_core_info_topic">{localized_city}</span> 
                                    <span className="main_info_core_info_topic">{this.addRatingStats(star_rating)}</span>
                                    <span className="main_info_core_info_topic">{reviews_count} reviews</span>
                                </div>

                                <div className="main_info_numberdProperties main_info_section">
                                    <p className="main_info_numberdProperties_title">
                                        {beds} beds
                                        <i className="title-icon fa fa-bed" aria-hidden="true"></i>                                        
                                    </p>
                                    <p className="main_info_numberdProperties_title">
                                        {bedrooms} bedrooms
                                        <i className="title-icon fa fa-university" aria-hidden="true"></i>                                        
                                    </p>
                                    <p className="main_info_numberdProperties_title">
                                        {bathrooms} bathrooms
                                        <i className="title-icon fa fa-venus-mars" aria-hidden="true"></i>
                                    </p>
                                    <p className="main_info_numberdProperties_title">
                                        {person_capacity} guests
                                        <i className="title-icon fa fa-users" aria-hidden="true"></i>  
                                    </p>  
                                </div>
                            </div>
                            
                            <div className="user">
                                <div className="user_profile_pic_wrapper">
                                    <img className="user_profile_pic" src={user.picture_url} />
                                </div>
                                <div className="user_name_wrapper">
                                    <p className="user_name"> {user.first_name} </p>
                                </div>
                            </div>
                        </main>

                        <hr/>
                        
                        <div className="reviews">
                            <PropertyReviews />
                        </div>
                        </PropertyMainStyle>
                    </div>    
            );

        } else {
            this.props.history.push('/');
        }
    }

    render(){
        return(
            <div>
                {this.showProperty()}
            </div>
        );
    }
}

const PropertyWithRouter = withRouter(Property);
export default PropertyWithRouter