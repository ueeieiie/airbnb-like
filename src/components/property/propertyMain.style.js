import styled from 'styled-components';

export default styled.div`
    margin: 20px;
    padding: 20px;

    main {
        display: flex;
    }

    .rating-icon {
        color: #008489;
        font-size: 15px;
    }

    .title-icon {
        color: #484848;
        font-size: 15px;
        padding: 0 10px;
    }

    .main_info {
        width: 600px;
    }
    
    .main_info_section {
        margin-bottom: 15px;
    }

    .main_info_property_title {
        padding: 0;
        margin: 5px 0;;
    }

    .main_info_numberdProperties {
        display: flex;
    }

    .main_info_core_info_topic{
        margin-right: 25px;
    }

    .main_info_numberdProperties_title {
        margin-right: 25px;   
    }
    
    .main_info_core_info {
        color: #484848;
        font-size: 0.8rem;
    }

    .user {
        display: flex;
        flex-direction: column;
        align-items: center;
        width: 100px;
    }

    .user_name {
        color: #484848;
        margin: 0;
    }

    .user_profile_pic_wrapper {
        width: 64px;
    }

    .user_profile_pic {
        border-radius: 50%;
        width: 100%;
    }

    .reviews {
        width: 60%;
    }
`;