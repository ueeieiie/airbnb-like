import styled from 'styled-components';

export default styled.div`
    background-repeat: no-repeat !important;
    background-size: cover !important;
    background-position: 50% 50% !important;
    height: 320px !important;
`;