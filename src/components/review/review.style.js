import styled from 'styled-components';

export default styled.div`
    padding: 10px 0;

    .review-header {
        display: flex;
    }

    .profile_pic_wrapper {
        width: 48px;
        margin-right: 15px;
    }

    .profile_pic {
        border-radius: 50%;
        width: 100%;
    }

    .username_wrapper {
        color: #484848;
        font-weight: bold;
    }

    .create_at_wrapper {
        font-size: 0.8rem;
        color: #484848;
        font-weight: 300;
    }

    .comment {
        color: #484848;
        font-weight: 300;
    }
`;
