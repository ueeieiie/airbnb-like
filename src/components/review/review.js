import React from 'react';

import {inject, observer } from 'mobx-react';

// Styles
import ReviewStyle from './review.style';

@inject('store')
@observer
export default class Review extends React.Component {
    render(){
        return(
            <ReviewStyle>
                <header className="review-header">
                    <div className="profile_pic_wrapper">
                        <img className="profile_pic" src={this.props.profile_pic} />
                    </div>
                    <div>
                        <div className="username_wrapper">
                            {this.props.username}
                        </div>
                        <div className="create_at_wrapper">
                            {this.props.created_at}
                        </div>
                    </div>
                    
                </header>

                <main>
                    <p className="comment"> {this.props.comment} </p>
                </main>

                <footer>
                    <hr className="hr" />
                </footer>
            </ReviewStyle>
        );
    }
}