// Styled-component
import styled from 'styled-components';

export const ThumbnailStyle = styled.div`
display: inline-block;
transition: all .1s ease;
margin-right: 15px;
margin-bottom: 25px;
width: 300px;
height: 270px;
overflow: hidden;

&:hover {
    box-shadow: 5px 5px 5px lightgray;
    cursor: pointer;
}

.thumbnail_img_wrapper {
    width: 300px;
    height: 200px;
    margin-bottom: 5px;
}

.thumbnail_img {
    width:100% ;
}

.city_tag, .address_tag {
    margin: 0;
}

.city_tag {
    text-transform: uppercase;
    font-size: 0.7rem;
    font-weight: bold;
    color: rgb(43, 148, 156);
    letter-spacing: 1px;
}

.address_tag {
    font-weight: bold;
    color: #484848;
}
`;