// React
import React from 'react';

// Mobx-react
import {inject, observer} from 'mobx-react';

// react-router-dom
import { Link, withRouter } from 'react-router-dom'

// Component's Style
import { ThumbnailStyle } from './thumbnail.style'

// Component
@inject('store')
@observer
class Thumbnail extends React.Component {
    setPropertyInfo = (property) => {
        this.props.store.updatePropertyInfo(property);
        this.props.store.updatePropertyReviews(property.listing.id)
        
        // redirect to the property page
        this.props.history.push('/property')
    }

    render(){
        const {thumbnail_url, city, public_address } = this.props.info.listing;
        const property = this.props.info;

        return (
                <ThumbnailStyle onClick={() => { this.setPropertyInfo(property) }}>
                    <div className="thumbnail_img_wrapper">
                        <img className="thumbnail_img" src={thumbnail_url} />
                    </div>
                    <div>
                        <h3 className="city_tag">{city}</h3>
                    </div>
                    <div className="address_tag_wrapper">
                        <p className="address_tag">{public_address}</p>
                    </div>
                </ThumbnailStyle>
        );
    }
}

const ThumbnailWithRouter = withRouter(Thumbnail);

export default ThumbnailWithRouter;