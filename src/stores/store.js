import { observable, action, computed } from 'mobx';

class AppStore {
    @observable citiesList = {
        london: { id: 1, title: 'London' },
        paris: { id: 2, title: 'Paris' },
        telaviv: { id: 3, title: 'Tel-Aviv' },
        newyork: { id: 4, title: 'New-York' },
    };

    @observable fetchedProperties = [];
    @observable propertyInfo = null;
    @observable propertyReviews = [];
    @observable isFetching = false;
    @observable selectedCity = null;

    @observable API = {
        client_id: `client_id=3092nxybyb0otqw18e8nh5nty`,
        search: `https://api.airbnb.com/v2/search_results?`,
        reviews: `https://api.airbnb.com/v2/reviews?`
    }

    @observable pagination = {
        current: 0,
        amount: 8,
        page: 1,
    }

    @computed get getProperties(){
        return this.fetchedProperties;
    }

    @action updateFetchedProperties (){
        const { current, amount} = this.pagination;
        const { client_id, search } = this.API;
        const limit = `_limit=${amount}`;
        let offset = `_offset=${current}`;

        const url = `${search}${client_id}&${limit}&${offset}`;

        fetch(url).then(response => response.json())
            .then(data => {
                this.fetchedProperties = [...data.search_results];                                     
            });
    }

    @action updateResultsByPage(direction){
        const { current, amount, page} = this.pagination;
        const { client_id, search } = this.API;
        
        const limit = `_limit=${amount}`;
        let currentPage;
        let url;

        if(this.isFetching) return;

        if(direction == 'next'){
            this.pagination.current = current + amount;
            currentPage = page + 1
            var offset = `_offset=${this.pagination.current}`;        
        } else {
            if(page !== 1) {
                this.pagination.current = current - amount;
                currentPage = page - 1
                var offset = `_offset=${this.pagination.current}`;           
            } 
            else return;
        }

        if(this.selectedCity){
            url = `${search}${client_id}&location=${this.selectedCity}&${limit}&${offset}`;
        } else {
            url = `${search}${client_id}&${limit}&${offset}`;
        }

        this.isFetching = true;
        fetch(url).then(response => response.json())
            .then(data => {
                this.fetchedProperties = [...data.search_results];
                this.pagination.page = currentPage; 
                this.isFetching = false;                                  
            });
    }

    @action updatePropertyInfo(property) {
        this.propertyInfo = property;
    }

    @action setSelectedCity(city) {
        console.log('selectedCity:', city)
        this.selectedCity = city;

        const { client_id, search } = this.API;
        const { current, amount } = this.pagination;
        
        const url = `${search}${client_id}&location=${city}&${limit}&${offset}`;
        const limit = `_limit=${amount}`;
        let offset = `_offset=${current}`;
        
        this.pagination.current = 0;
        this.pagination.page = 1;
        
        fetch(url).then(response => response.json())
            .then(data => {
                 this.fetchedProperties = data.search_results;
            });
    }

    @action updatePropertyReviews (listing_id){
        const {reviews, client_id} = this.API
        const url = `${reviews}${client_id}&role=all&listing_id=${listing_id}`;
        
        fetch(url).then(response => response.json())
            .then(data => {
                this.propertyReviews = [...data.reviews]
            });
    }
}

const appStore = new AppStore();

export default appStore;